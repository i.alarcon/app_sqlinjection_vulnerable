from flask import Blueprint
from flask import render_template, request, flash, redirect, url_for
from .forms import RegisterForm, BusquedaForm
from .models import *
from .messages import *
from .tablas import Results

page = Blueprint('page', __name__)

@page.route('/', methods=['GET', 'POST'])
def index():
    form = RegisterForm(request.form)

    if request.method == 'POST':
        User.create_element(form.name.data, form.email.data, form.password.data)
        flash(USER_CREATED)
        return redirect(url_for('page.index'))

    return render_template('index.html', title='Registro', form=form)

@page.route('/busqueda', methods=['GET', 'POST'])
def busqueda():

    form = BusquedaForm(request.form)
    if request.method == 'post':
        return search_results(form)

    return render_template('busqueda.html', title='Busqueda', form=form)

@page.route('/resultados')
def search_results(form):
    resultados = []
    string_busqueda = form.data['search']

    if form.data['search'] == "":
        User.buscar_elemento()
    if not resultados:
        flash('Resultado no encontrado')
        return redirect(url_for('page.busqueda'))
    else:
        resultados = User.query.all()
        table = Results(resultados)
        table.border = True
        return render_template('resultados.html', title='Resultados', table=table)


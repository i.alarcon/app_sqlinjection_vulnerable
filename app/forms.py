from wtforms import Form
from wtforms import validators
from wtforms import StringField, PasswordField, BooleanField
from wtforms.fields.html5 import EmailField

class RegisterForm(Form):
    name = StringField('Name')
    email = EmailField('Email')
    password = PasswordField('Password', [validators.EqualTo('confirm_password', message='Las contrase;as no coinciden')])
    confirm_password = PasswordField("Confirm Password")
    accept = BooleanField('', [

    ])

class BusquedaForm(Form):
    search = StringField('')

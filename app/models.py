from flask_login import UserMixin


from . import db

class User(UserMixin, db.Model):
    __tablename__ = 'usuarios'
    id = db.Column(db.Integer, db.Sequence('SEQ_USUARIOS'), primary_key=True, nullable=False)
    username = db.Column(db.String(50), nullable=False)
    email = db.Column(db.String(100), nullable=False)
    password = db.Column(db.String(25), nullable=False)

    #INSERT
    @classmethod
    def create_element(cls, username, password, email):
        user = User(username=username, email=email, password=password)

        db.session.add(user)
        db.session.commit()

        return user

    #SELECT
    def buscar_elemento(cls):
        query = db.session.query_property(User)
        results = []
        results = query.all()
        return results

    def todos_los_elementos(cls):
        return User.query.all()
# app_sqlinjection_vulnerable

App developed with Flask that allows to make sqlinjection in the register form and search.

1. pip install -r requirements.text
2. Go to config.py and edit the Database URI
3. python manage.py runserver o python3 manage.py runserver
4. Go to 127.0.0.1:5000
    The fields in the register form don't have validators and the password is not encrypted.
    The search bar, allows you to insert sql statements and return the information
    The app don't have security and don't create a token to the users

Solution

1. use the werkzeug module to improve the security generating hash functions to the passwords
    https://pypi.org/project/Werkzeug/

2. use validators from the wtforms module, to limit the characters of the fields and have the security that the user insert the fields do you want. for example to verify an email field:

email = EmailField('Email', [
        validators.length(min=6, max=100),
        validators.Required(message='El email es requerido.'),
        validators.Email(message='Ingre un email valido.')
    ])
3. Configure a secret key in config.py to generate a CSRF toke for the user sessions
4. Use an user that only can make the changes do you want in the database, don't use sysdba or root

